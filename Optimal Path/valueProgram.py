import numpy as np
# grid = [[0, 0, 1, 0, 0, 0],
#         [0, 0, 1, 0, 0, 0],
#         [0, 0, 1, 0, 0, 0],
#         [0, 0, 0, 0, 1, 0],
#         [0, 0, 1, 1, 1, 0],
#         [0, 0, 0, 0, 1, 0]]
grid = [[0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 1, 0, 0, 0, 0],
        [0, 0, 0, 0, 1, 0]]
        ##THE POSITION (0,5) is erraneous
# grid = [[0, 1, 0, 0, 0, 0],
# [0, 1, 0, 0, 0, 0],
# [0, 1, 0, 0, 0, 0],
# [0, 1, 0, 0, 0, 0],
# [0, 0, 0, 0, 1, 0]]
# grid = [[0 , 0 , 0],
#         [0 , 1 , 0],
#         [0 , 0 , 0]] 
grid = np.array(grid)
print(grid)
init = [0,0]
goal = [len(grid)-1, len(grid[0])-1]
delta = [[-1,0],
        [0,-1],
        [1,0],
        [0,1]]

# delta_name = ['^', '<', 'v', '>']
delta_name = ['v', '>' , '^' , '<' ]
cost = 1
closed = np.zeros([len(grid), len(grid[0])])
gvalues = np.ones([len(grid), len(grid[0])])
gvalues*=-1

gvalues = gvalues.astype(int)

expand = np.ones([len(grid), len(grid[0])])
direction = [[' ' for col in range(len(grid[0]))] for row in range(len(grid))]
# expand *= -1
expand *= 99
expand = expand.astype(int)
policy = [[' ' for row in range(len(grid[0]))] for col in range(len(grid))]
policy = np.array(policy)
count = 0
closed[init[0],init[1]] = 1
x = init[0]
y = init[1]
g = 0
expand[x][y] = 0
open = [[g, x, y]]
found = False
resign = False
while found is False and resign is False:
    if(len(open)==0):
        resign = True
        print('Fail')
    else:
        open.sort()
        open.reverse()
        next = open.pop()

        x = next[1]
        y = next[2]
        g = next[0]
        # expand[x][y] = g loop breaks before assigning
        # gvalues[x][y]= g

        count+=1
        # print(x,y)
        if (x == goal[0]) and (y == goal[1]):
            found = True
            print('Found!')
        else:
            for i in range(len(delta)):
                x2 = x + delta[i][0]
                y2 = y + delta[i][1]
                # path[x][y] = delta_name[i]
                # print(x2,y2)
                if x2>=0 and x2<len(grid) and y2>=0 and y2<len(grid[0]):
                    if (closed[x2][y2] == 0) and (grid[x2][y2]==0 or grid[x2][y2] == 9):
                        g2 = g+cost
                        gvalues[x2][y2]= g2
                        open.append([g2, x2, y2])
                        closed[x2][y2]=1
                        expand[x2][y2] = g2
                        # if(delta_name[i] == '^'):
                        #     policy[x2][y2] = 'v'
                        # elif(delta_name[i] == 'v'):
                        #     policy[x2][y2] = '^'
                        # elif(delta_name[i] == '<'):
                        #     policy[x2][y2] = '>'
                        # elif(delta_name[i] == '>'):
                        #     policy[x2][y2] = '<'
                        policy[x][y] = delta_name[i]
print(policy)
print(expand)