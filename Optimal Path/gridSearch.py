import numpy as np


def search():
	grid = [[0,0,1,0,0,0],
			[0,0,1,0,0,0],
			[0,0,0,0,1,0],
			[0,0,1,1,1,0],
			[0,0,0,0,1,0]]

	init = [0,0]
	goal = [len(grid)-1, len(grid[0])-1]
	print(goal)

	delta = [[-1,0],
			[0,-1],
			[1,0],
			[0,1]]
	cost = 1 
	# delta_name = ['^', '<', 'v', '>']

	# closed = np.copy(grid)
	closed = np.zeros([len(grid), len(grid[0])])
	print(closed)
	# print(grid)
	closed[init[0]][init[1]] = 1
	x= init[0]
	y= init[1]
	g = 0
	open = [[g,x,y]]
	found = False
	resign = False
	# for i 
	while ((found is False) and (resign is False)):
		if (len(open) == 0):
			resign = True
			print('Fail')
		else:
			open.sort()
			open.reverse()
			next = open.pop()
			print(next)
			x = next[1]
			y = next[2]
			g = next[0]
			# print('ff')
			if (x == goal[0] and y == goal[1]):
				found = True
				# print(x,y,g)
				# print('f')
				# print(next)
			else:
				for i in range(len(delta)):
					x2 = x + delta[i][0]
					y2 = y + delta[i][1]
					# print('tt')
					if (x2 >= 0 and x2 < len(grid)) and (y2 >= 0 and y2 < len(grid[0])):
						if closed[x2][y2] == 0 and grid[x2][y2] == 0:
							g2 = g + cost
							open.append([g2,x2,y2])
							print([g2,x2,y2])
							# print("e")
							closed[x2][y2] = 1
search()
						