#include <functional>
#include <iostream>
#include "behaviourCost2.h"
#include "cmath"


using namespace std;

float inefficiency_cost(int target_speed, int intended_lane, int final_lane, vector<int> lane_speeds) {
    /*
    Cost becomes higher for trajectories with intended lane and final lane that have traffic slower than target_speed.
    */
    
    //TODO: Replace cost = 0 with an appropriate cost function.
    float cost = 0;
    float speed_intended = lane_speeds[intended_lane];
    float speed_final = lane_speeds[final_lane];
    cost = (2*target_speed - speed_final - speed_intended)/target_speed;

    return cost;
}